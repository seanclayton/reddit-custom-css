import React from "react";
import createReactContext from "create-react-context";

const Context = createReactContext({});

export const customStyleMixin = ({ customStyles }) => customStyles;

export const withCustomStyles = key => Component =>
  class extends React.Component {
    render() {
      return (
        <Context.Consumer>
          {customStyles => (
            <Component {...this.props} customStyles={customStyles[key]} />
          )}
        </Context.Consumer>
      );
    }
  };

export default Context;
