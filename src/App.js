import React, { Component } from "react";
import debounce from "lodash.debounce";
import RedditPostList from "components/RedditPostList";
import CustomCSSForm from "components/CustomCSSForm";
import CustomStyles from "CustomStyles";
import styled from "styled-components";
import { allPosts } from "posts";
import { setDisplayName } from "recompose";

const AppWrapper = setDisplayName("AppWrapper")(styled.main`
  display: grid;
  grid-template-areas: "custom-css post-list";
  grid-template-columns: minmax(min-content, 320px) minmax(min-content, 1fr);
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen,
    Ubuntu, Cantarell, "Open Sans", "Helvetica Neue", sans-serif;
  background-color: #edeff1;
  position: fixed;
  overflow-y: scroll;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`);

const storage = window.localStorage;

class App extends Component {
  state = {
    customStyles: storage.getItem("customStyles")
      ? JSON.parse(storage.getItem("customStyles"))
      : {},
  };
  updateComponentStyle = (key, newValue) => {
    this.setState(
      state => ({
        customStyles: {
          ...state.customStyles,
          [key]: newValue,
        },
      }),
      () => {
        storage.setItem(
          "customStyles",
          JSON.stringify(this.state.customStyles),
        );
      },
    );
  };
  render() {
    return (
      <AppWrapper>
        <CustomStyles.Provider value={this.state.customStyles}>
          <CustomCSSForm
            updateComponentStyle={debounce(this.updateComponentStyle, 300)}
          />
          <RedditPostList posts={allPosts} />
        </CustomStyles.Provider>
      </AppWrapper>
    );
  }
}

export default App;
