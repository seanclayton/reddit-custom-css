import faker from "faker";
import { v4 as uuid } from "uuid";

const emojiList = ["😎", "😄", "😭", "💩", "🎷🐢"];

const capitalizeString = string =>
  `${string.charAt(0).toUpperCase()}${string.slice(1)}`;

const genTag = () => ({
  label: capitalizeString(faker.lorem.word()),
  emoji: emojiList[faker.random.number({ min: 0, max: emojiList.length - 1 })],
});

const genPost = () => ({
  id: uuid(),
  votes: faker.random.number({ min: 0, max: 100 }),
  author: faker.internet.userName(),
  title: faker.lorem.sentence(),
  thumbnail: `https://loremflickr.com/160/100/beagle?random=${Math.random()}`,
  createdAt: faker.date.recent(7),
  tag: faker.random.boolean() && genTag(),
  numberOfComments: faker.random.number({ min: 0, max: 100 }),
});

export const createPosts = (numberOfPosts = 5) =>
  Array.from(Array(numberOfPosts)).map(genPost);

export const allPosts = createPosts();
