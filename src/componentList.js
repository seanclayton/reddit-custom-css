export default [
  "reddit.post.list",
  "reddit.post.list.item",
  "post.vote.well",
  "vote.button",
  "post.thumbnail",
  "post.header",
  "post.title",
  "post.tag",
  "post.by.line",
  "post.actions",
  "post.actions.button",
];
