import React from "react";
import styled from "styled-components";
import { withCustomStyles, customStyleMixin } from "CustomStyles";

const TagWrapper = styled.div`
  display: inline-block;
  font-size: 0.75em;
  padding: 0 0.25rem;
  background-color: orangered;
  margin-left: 0.25rem;
  border-radius: 3px;
  color: white;
  font-weight: normal;
  ${customStyleMixin};
`;

const PostTag = ({ label, emoji, customStyles }) => (
  <TagWrapper customStyles={customStyles}>
    {label} {emoji}
  </TagWrapper>
);

export default withCustomStyles("post.tag")(PostTag);
