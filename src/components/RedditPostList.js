import React from "react";
import styled from "styled-components";
import { withCustomStyles, customStyleMixin } from "CustomStyles";
import { setDisplayName } from "recompose";
import RedditPostListItem from "./RedditPostListItem";

const Wrapper = styled.ul`
  grid-area: post-list;
  position: sticky;
  margin: 0;
  padding: 1em;
  list-style-type: none;
  ${customStyleMixin};
`;

const RedditPostList = ({ posts, customStyles }) => (
  <Wrapper customStyles={customStyles}>
    {posts.map(post => <RedditPostListItem key={post.id} post={post} />)}
  </Wrapper>
);

export default setDisplayName("RedditPostList")(
  withCustomStyles("reddit.post.list")(RedditPostList),
);
