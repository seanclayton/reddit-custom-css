import React from "react";
import styled from "styled-components";
import { withCustomStyles, customStyleMixin } from "CustomStyles";

const Wrapper = styled.div`
  --post-thumbnail-border-color: #f3f3f3;
  grid-area: thumbnail;
  padding: 0.5em 0;
  ${customStyleMixin};
`;

const Thumbnail = styled.div`
  width: 84px;
  height: 56px;
  border-radius: 3px;
  ${({ url }) => `background-image: url("${url}")`};
  background-size: cover;
  border: 1px solid var(--post-thumbnail-border-color);
`;

const PostThumbnail = ({ thumbnail, customStyles }) => (
  <Wrapper customStyles={customStyles}>
    <Thumbnail url={thumbnail} />
  </Wrapper>
);

export default withCustomStyles("post.thumbnail")(PostThumbnail);
