import styled from "styled-components";
import { customStyleMixin } from "CustomStyles";

const PostTitle = styled.h3`
  display: inline;
  vertical-align: middle;
  overflow-wrap: break-word;
  font-size: 15px;
  line-height: 22px;
  margin: 0;
  color: #373c3f;
  font-weight: 500;
  ${customStyleMixin};
`;

export default PostTitle;
