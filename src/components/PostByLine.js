import React from "react";
import styled from "styled-components";
import { withCustomStyles, customStyleMixin } from "CustomStyles";

const Wrapper = styled.div`
  grid-area: post-byline;
  display: flex;
  flex-direction: row;
  align-items: center;
  font-size: 11px;
  line-height: 1;
  ${customStyleMixin};
`;

const PostByLine = ({ author, createdAt, customStyles }) => (
  <Wrapper customStyles={customStyles}>
    Posted by u/{author} on {createdAt.toLocaleString()}
  </Wrapper>
);

export default withCustomStyles("post.by.line")(PostByLine);
