import React from "react";
import styled from "styled-components";
import componentList from "componentList";
import ComponentEditor from "./ComponentEditor";

const capitalizeString = string =>
  `${string.charAt(0).toUpperCase()}${string.slice(1)}`;

const dotToPascal = string =>
  string
    .split(".")
    .map(capitalizeString)
    .join("");

const components = componentList.map(key => ({
  key,
  name: dotToPascal(key),
}));

const Wrapper = styled.aside`
  grid-area: custom-css;
  position: relative;
  display: flex;
  flex-direction: column;
  height: 100vh;
  min-height: 100%;
  padding-left: 1em;
`;

const EditorList = styled.div`
  ${({ open }) => (open ? "opacity: 1;" : "opacity: 0; pointer-events: none;")};
  flex: 1;
  overflow-y: scroll;
  background: transparent;
`;

const ToggleButton = styled.button`
  align-self: flex-end;
  font-size: 1.5em;
  padding: 0;
  background: transparent;
  cursor: pointer;
  border: none;
`;

class CustomCSSForm extends React.Component {
  state = {
    open: false,
  };
  toggleOpen = () => {
    this.setState(state => ({
      open: !state.open,
    }));
  };
  onChange = (key, newValue) => {
    this.props.updateComponentStyle(key, newValue);
  };
  render() {
    const initialStyles = localStorage.getItem("customStyles")
      ? JSON.parse(localStorage.getItem("customStyles"))
      : null;
    return (
      <Wrapper>
        <ToggleButton onClick={this.toggleOpen}>
          {this.state.open && "🎶"}🎷🐢
        </ToggleButton>
        <EditorList open={this.state.open}>
          {components.map(({ key, name }) => (
            <ComponentEditor
              defaultValue={initialStyles && initialStyles[key]}
              key={key}
              componentKey={key}
              componentName={name}
              onChange={this.onChange}
            />
          ))}
        </EditorList>
      </Wrapper>
    );
  }
}

export default CustomCSSForm;
