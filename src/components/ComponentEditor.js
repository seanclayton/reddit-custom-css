import React from "react";
import styled from "styled-components";
import MonacoEditor from "react-monaco-editor";

const Editor = styled(MonacoEditor)`
  .margin-view-overlays {
    display: none;
  }
`;

const ComponentEditor = ({
  defaultValue,
  value,
  onChange,
  componentKey,
  componentName,
}) => (
  <React.Fragment>
    <h3>
      <code>{`<${componentName} />`}</code>
    </h3>
    <Editor
      theme="vs"
      language="plaintext"
      defaultValue={defaultValue}
      value={value}
      onChange={newValue => onChange(componentKey, newValue)}
      editorDidMount={editor => {
        editor.getModel().updateOptions({
          tabSize: 2,
        });
      }}
      options={{
        fontSize: 12,
        lineDecorationsWidth: 0,
        scrollBeyondLastLine: false,
        wordWrap: "on",
        renderIndentGuides: true,
        useTabStops: false,
        minimap: {
          enabled: false,
        },
      }}
      height="320"
      requireConfig={{
        url:
          "https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.5/require.min.js",
        paths: {
          vs: "https://unpkg.com/monaco-editor/min/vs/",
        },
      }}
    />
  </React.Fragment>
);

export default ComponentEditor;
