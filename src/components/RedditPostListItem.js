import React from "react";
import styled from "styled-components";
import { setDisplayName } from "recompose";
import { withCustomStyles, customStyleMixin } from "CustomStyles";
import PostHeader from "./PostHeader";
import PostVoteWell from "./PostVoteWell";
import PostThumbnail from "./PostThumbnail";
import PostActions from "./PostActions";

const PostWrapper = setDisplayName("PostWrapper")(styled.li`
  --hover-border: 1px solid #a3a7a8;
  background-color: white;
  border: 1px solid #d3d6da;
  color: #a4a7a8;
  margin-top: -1px;
  display: grid;
  grid-column-gap: 0.5em;
  grid-template-areas:
    "vote-well thumbnail post-title"
    "vote-well thumbnail post-byline"
    "vote-well thumbnail post-actions";
  grid-template-columns: max-content max-content 1fr;
  grid-template-rows: repeat(3, auto-fill);

  &:not(:hover) {
    z-index: 0;
  }

  &:hover {
    position: relative;
    border: var(--hover-border);
    cursor: pointer;
  }

  ${customStyleMixin};
`);

class RedditPostListItem extends React.Component {
  state = {
    voted: null,
  };
  upvote = () => {
    this.setState({
      voted: "UP",
    });
  };
  downvote = () => {
    this.setState({
      voted: "DOWN",
    });
  };
  unvote = () => {
    this.setState({
      voted: null,
    });
  };
  render() {
    return (
      <PostWrapper customStyles={this.props.customStyles}>
        <PostHeader post={this.props.post} />
        <PostVoteWell
          unvote={this.unvote}
          upvote={this.upvote}
          downvote={this.downvote}
          voted={this.state.voted}
          votes={this.props.post.votes}
        />
        <PostThumbnail thumbnail={this.props.post.thumbnail} />
        <PostActions numberOfComments={this.props.post.numberOfComments} />
      </PostWrapper>
    );
  }
}

export default withCustomStyles("reddit.post.list.item")(RedditPostListItem);
