import React from "react";
import styled from "styled-components";
import { withCustomStyles, customStyleMixin } from "CustomStyles";
import PostTitle from "./PostTitle";
import PostTag from "./PostTag";
import PostByLine from "./PostByLine";

const CustomTitle = withCustomStyles("post.title")(PostTitle);

const PostTitleArea = styled.div`
  grid-area: post-title;
  margin: 0.5em 0.5em 0.25em 0;
  ${customStyleMixin};
`;

export default withCustomStyles("post.header")(({ post, customStyles }) => (
  <React.Fragment>
    <PostTitleArea customStyles={customStyles}>
      <CustomTitle>{post.title}</CustomTitle>
      {post.tag && (
        <React.Fragment>
          <wbr />
          <PostTag label={post.tag.label} emoji={post.tag.emoji} />
        </React.Fragment>
      )}
    </PostTitleArea>
    <PostByLine author={post.author} createdAt={post.createdAt} />
  </React.Fragment>
));
