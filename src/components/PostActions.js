import React from "react";
import styled from "styled-components";
import fontawesome from "@fortawesome/fontawesome";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {
  faShare,
  faComments,
  faEllipsisH,
} from "@fortawesome/fontawesome-free-solid";
import { withCustomStyles, customStyleMixin } from "CustomStyles";

fontawesome.library.add(faShare, faComments, faEllipsisH);

const Wrapper = styled.aside`
  grid-area: post-actions;
  display: flex;
  align-items: flex-end;
  padding: 0 0 0.25em;
  ${customStyleMixin};
`;

const ActionButtonIcon = styled(FontAwesomeIcon)`
  margin-right: 0.25em;
`;

const ActionButton = withCustomStyles("post.actions.button")(styled.button`
  display: flex;
  cursor: pointer;
  border-radius: 3px;
  font-size: 0.75em;
  font-weight: bold;
  background: transparent;
  border: none;
  color: inherit;
  padding: 0.333em;
  margin-right: 1em;

  &:hover {
    background-color: #0000000f;
  }

  ${customStyleMixin};
`);

const PostActions = ({ numberOfComments, customStyles }) => (
  <Wrapper customStyles={customStyles}>
    <ActionButton>
      <ActionButtonIcon icon="comments" />
      {numberOfComments} Comments
    </ActionButton>
    <ActionButton>
      <ActionButtonIcon icon="share" />
      Share
    </ActionButton>
    <ActionButton>
      <ActionButtonIcon icon="ellipsis-h" />
    </ActionButton>
  </Wrapper>
);

export default withCustomStyles("post.actions")(PostActions);
