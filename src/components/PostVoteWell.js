import React from "react";
import styled from "styled-components";
import { withCustomStyles, customStyleMixin } from "CustomStyles";
import VoteButton from "./VoteButton";

const Wrapper = styled.span`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex-direction: column;
  width: 24px;
  grid-area: vote-well;
  padding: 0.5em;
  background-color: #f7f8f9;
  border-right: 1px solid #f2f3f3;
  ${customStyleMixin};
`;

const VoteCount = styled.span`
  color: ${({ status }) => {
    switch (status) {
      case "UP":
        return "orangered";
      case "DOWN":
        return "#7491fa";
      default:
        return "inherit";
    }
  }};
`;

VoteCount.passProps = false;

const PostVoteWell = ({
  unvote,
  upvote,
  downvote,
  voted,
  votes,
  customStyles,
}) => {
  let votesDisplay;
  switch (voted) {
    case "UP":
      votesDisplay = votes + 1;
      break;
    case "DOWN":
      votesDisplay = votes - 1;
      break;
    case null:
    default:
      votesDisplay = votes;
      break;
  }
  return (
    <Wrapper customStyles={customStyles}>
      <VoteButton
        icon="arrow-up"
        action={voted === "UP" ? unvote : upvote}
        active={voted === "UP"}
        activeColor="orangered"
      />
      <VoteCount status={voted}>{votesDisplay}</VoteCount>
      <VoteButton
        icon="arrow-down"
        action={voted === "DOWN" ? unvote : downvote}
        active={voted === "DOWN"}
        activeColor="#7491fa"
      />
    </Wrapper>
  );
};

export default withCustomStyles("post.vote.well")(PostVoteWell);
