import React from "react";
import styled from "styled-components";
import omit from "lodash.omit";
import fontawesome from "@fortawesome/fontawesome";
import { mapProps } from "recompose";
import { faArrowUp, faArrowDown } from "@fortawesome/fontawesome-free-solid";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import { withCustomStyles, customStyleMixin } from "CustomStyles";

fontawesome.library.add(faArrowUp, faArrowDown);

const Button = styled.button`
  cursor: pointer;
  border: none;
  background: transparent;
  padding: 0.25em;
  ${customStyleMixin};
`;

const omitProps = keys => mapProps(props => omit(props, keys));

const Icon = styled(omitProps(["active", "activeColor"])(FontAwesomeIcon))`
  color: ${({ active, activeColor }) => (active ? activeColor : "inherit")};
`;

const VoteButton = ({ action, icon, active, activeColor, customStyles }) => (
  <Button customStyles={customStyles} onClick={action}>
    <Icon size="lg" active={active} activeColor={activeColor} icon={icon} />
  </Button>
);

export default withCustomStyles("vote.button")(VoteButton);
